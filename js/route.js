layui.use([ 'table', 'form', 'layer',  'laytpl', 'element','laydate' ], function() {
	var table = layui.table, form = layui.form, 
		layer = layui.layer, $ = layui.jquery,
		element = layui.element,laydate = layui.laydate;
		
		//页面加载table数据
		querylist();
		
		//点击事件条件查询
		$("#btnselbackdepart").click(function(){
			querylist();
		});
		
		function querylist(){
			var routename=$("#routename").val().trim();
			var start=$("#start").val().trim();
			var qend=$("#qend").val().trim();
			var param = "?routename="+routename+"&start="+start+"&ends="+qend;
			/*加载表格*/
			table.render({
				elem : '#table',
				id:'table',
				url : urlapi+'route/getroutelist'+param,
				title : '',
				height : "full-180",
				skin : 'line',
				curr:1,
				even : true,
				cols : [ 
					 [{
						title : '编号',
						field : 'routeid',
						align : 'center',
						hide : true,
					},{
						title : '线路名称',
						field : 'routename',
						align : 'center',
					},
					{
						title : '起点',	
						field : 'start',
						align : 'center',
					},
					{
						title : '终点',
						field : 'ends',
						align : 'center',
					},
					{
						title : '出发时间',
						field : 'statetime',
						align : 'center',
					}, {
						title : '到达时间',	
						field : 'endtime',
						align : 'center',
					},
					{
						title : '终点签到',	
						field : 'endIsdit',
						align : 'center',
					},
					{
						title : '备注',	
						field : 'routenote',
						align : 'center',
					},
					 {
						title: '操作',
						toolbar: '#barDemo',
						align: 'center',
						width : 250
					}] 
				],
				page: {
					layout: ['prev', 'page', 'next', 'skip', 'count', 'limit'],
					groups: 5,
					limit: 10,
					limits: [10, 20, 30, 40, 50],
					theme: '#1E9FFF',						
				 },
			 });
		}
				
		//table数据删除与修改		
		table.on('tool(table)', function(obj){ //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
		  var data = obj.data; //获得当前行数据
		  var layEvent = obj.event;
		  //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
		  var tr = obj.tr; //获得当前行 tr 的DOM对象
		  if(layEvent == 'del'){ //删除
			layer.confirm('真的删除行么', function(index){
			  obj.del(); //删除对应行（tr）的DOM结构，并更新缓存
			  layer.close(index);
			  //向服务端发送删除指令
				$.post(urlapi+'route/deleteroute',{routeid:obj.data.routeid},function(flag){
					if(flag){
						layer.msg(flag.msg,{icon:6}); //表格刷新
						table.reload('table'); //重载表格	
					}else{
						layer.msg(flag.msg,{icon:5});
					}
				});
			});
		   } else if(layEvent === 'system'){ //编辑
			  //do something
			 //脚本编辑弹出层
			       var name = encodeURIComponent(data.toolName);
			       //alert(name);
			       layer.open({
			             type: 2,
			             title: '添加打卡点',
			             shadeClose: true,
			             shade: 0.5,
			             closeBtn:'1',//右上角xx关闭
			             area: ['1000px','500px'],
			             content:'addClock.html',
			             yes: function(index, layero){
						           //点击确认触发 iframe 内容中的按钮提交
						           var submit = layero.find('iframe').contents().find("#layuiadmin-app-form-submit");
						           submit.click();
						           //表格刷新
			        	   table.reload('table');
						        },
			             success : function(layero, index){
							            var body = layui.layer.getChildFrame('body', index);
							            if(layEvent === 'system'){
							                // 取到弹出层里的元素，并把编辑的内容放进去
							                var oldid = obj.data.routeid;
											sessionStorage.setItem('routeid',oldid);
											console.log(oldid);
							                body.find("#oldid").val(obj.data.routeid);  //将选中的数据的id传到编辑页面的隐藏域，便于根据ID修改数据
							                // 记得重新渲染表单
			              		form.render();
							            }
							            setTimeout(function(){
							                layui.layer.tips('点击此处返回用户列表', '.layui-layer-setwin .layui-layer-close', {
							                    tips: 3
							                });
							            },100)
							        }
			            
			       });
			        //同步更新缓存对应的值
			  obj.update({
			    username: '123'
			    ,title: 'xxx'
			  });
			  }else if(layEvent == 'edit'){ //编辑
			//do something
		   //脚本编辑弹出层
				 var name = encodeURIComponent(data.toolName);
				 //alert(name);
				 layer.open({
					   type: 2,
					   title: '修改信息',
					   shadeClose: true,
					   shade: 0.5,
					   closeBtn:'1',//右上角xx关闭
					   area: ['500px','500px'],
					   content:'edit_Route.html',
					   btn: ['确定', '取消'],
					   yes: function(index, layero){
					   //点击确认触发 iframe 内容中的按钮提交
					   var submit = layero.find('iframe').contents().find("#btn_update");
					   submit.click();
					   //表格刷新
					   table.reload('table');
					},
					   success : function(layero, index){
							var body = layui.layer.getChildFrame('body', index);
							if(layEvent == 'edit'){
								// 取到弹出层里的元素，并把编辑的内容放进去
								body.find("#routeid").val(obj.data.routeid);  //将选中的数据的id传到编辑页面的隐藏域，便于根据ID修改数据
								body.find("#routename").val(obj.data.routename); 
								body.find("#statetime").val(obj.data.statetime);
								body.find("#endtime").val(obj.data.endtime);
								body.find("#endIsdit").val(obj.data.endIsdit);
								body.find("#start").val(obj.data.start);
								body.find("#ends").val(obj.data.ends);
								body.find("#routenote").val(obj.data.routenote);
								// 记得重新渲染表单
								form.render();
							}
							setTimeout(function(){
								layui.layer.tips('点击此处返回用户列表', '.layui-layer-setwin .layui-layer-close', {
									tips: 3
								});
							},100)
						}
				 });
				  //同步更新缓存对应的值
			obj.update({
			  username: '123'
			  ,title: 'xxx'
			});
		  }
		});		
			
		//添加一条方法开始
		$("#btn_add").click(function(){
			var routename=$("#addroutename").val();
			var start=$("#addstart").val();
			var ends=$("#ends").val();	
			var statetime=$("#statetime").val();
			var endtime=$("#endtime").val();
			var routenote=$("#routenote").val();	
			var obj = {
						"routename":routename,
						"start":start,
						"ends":ends,	
						"statetime":statetime,
						"endtime":endtime,
						"routenote":routenote,	
						};
						
			$.ajax({
					url : urlapi+'route/addroute',
					type : "POST",
					data : obj, //直接传对象参数 
					dataType : 'json',
					success : function(data) {
						if(data.code == 0){
							layer.msg(data.msg,{icon:6}); 
							table.reload('table');
						}else{
							layer.msg(data.msg,{icon:5});
						}
						setTimeout(function(){
							parent.layer.close(index); //再执行关闭 
							parent.layui.table.reload('table'); //重载表格
						},1*1000)
					},
					error:function(data){
						layer.msg(data.msg,{icon:5});
					},
					/* success : function(data) {
						layer.msg(data.msg,{icon:6}); //表格刷新
						table.reload('table'); //重载表格	
					},
					error:function(){
						layer.msg(data.msg,{icon:5}); //表格刷新;
					}  */
				});	
			});
			return false;
		});
		//添加结束