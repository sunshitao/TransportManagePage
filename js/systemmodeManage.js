layui.use([ 'table', 'form', 'layer', 'laydate', 'laytpl', 'element' ], function() {
		var table = layui.table, form = layui.form, 
			layer = layui.layer, $ = layui.jquery,
			laydate = layui.laydate, laytpl = layui.laytpl,
			element = layui.element;
	
		/*加载表格*/
		table.render({
			elem : '#systemModel',
			id:'systemModel',
			url : urlapi+'systemmodel/getallsystemmodelone',
			title : '后台窗口数据表',
			height: "full-130",
			skin : 'line',
			even : true,
			cols : [ 
			     [ 
			     {
					field : 'sysid',
					title : 'ID',
					hide: true ,
					width : 50,
				}, {
					field : 'sysname',
					align : 'center',
					title : '菜单名称',
					width : 230,
				}, {
					field : 'chinesename',
					align : 'center',
					title : '展示名称',
					width : 250,
				}, {
					field : 'imageurl',
					title : '展示图标',
					align : 'center',
					
				},{
					field : 'navurl',
					align : 'center',
					title : '跳转页面',
					width : 250,
				},{
					title : '操作',
					toolbar : '#barDemo',
					align : 'center',
					width : 300,
				} ] 
			 ],
			 page: {
					layout: ['prev', 'page', 'next', 'skip', 'count', 'limit'],
					groups: 5,
					limit: 10,
					limits: [10, 20, 30, 40, 50],
					theme: '#1E9FFF',						
			 },
		});
		
		$("#selecet").click(function(){
   		var sysname=$("#SystemModelselecet").val().trim();
   		var param = '?sysname='+sysname;
		table.render({
			elem : '#systemModel',
			id:'systemModel',
			url : urlapi+'systemmodel/getallsystemmodelone' + param,
			title : '后台管理员用户数据表',
			height: "full-130",
			skin : 'line',
			even : true,
			cols : [ 
			     [ 
			     {
					field : 'sysid',
					title : 'ID',
					hide: true ,
					width : 50,
				}, {
					field : 'sysname',
					align : 'center',
					title : '菜单名称',
					width : 230,
				}, {
					field : 'chinesename',
					align : 'center',
					title : '展示名称',
					width : 250,
				}, {
					field : 'imageurl',
					title : '展示图标',
					align : 'center',
					
				},{
					field : 'navurl',
					align : 'center',
					title : '跳转页面',
					width : 250,
				},{
					title : '操作',
					toolbar : '#barDemo',
					align : 'center',
					width : 300,
				} ] 
			 ],
			 page: {
					layout: ['prev', 'page', 'next', 'skip', 'count', 'limit'],
					groups: 5,
					limit: 10,
					limits: [10, 20, 30, 40, 50],
					theme: '#1E9FFF',						
			 },
			});
		});	
		/*加载表格结束*/
		//table监听工具条
		table.on('tool(systemModel)', function(obj){ //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
		  var data = obj.data; //获得当前行数据
		  var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
		  var tr = obj.tr; //获得当前行 tr 的DOM对象
		  if(layEvent === 'delete'){ //删除
		    layer.confirm('真的删除行么', function(index){
		      obj.del(); //删除对应行（tr）的DOM结构，并更新缓存
		      layer.close(index);
		      //向服务端发送删除指令
		      	$.post(urlapi+"systemmodel/deletesystemmodel",{sysid:obj.data.sysid},function(flag){
	    			if(flag){
	    				layer.msg("刪除成功",{icon:6}); //表格刷新
				        table.reload('test'); //重载表格	
	    			}else{
	    				layer.msg("刪除失敗",{icon:5});
	    			}
		    	});
		    });
		  } else if(layEvent === 'system'){ //编辑
		    //do something
		   //脚本编辑弹出层
	             var name = encodeURIComponent(data.toolName);
	             //alert(name);
	             layer.open({
		               type: 2,
		               title: '添加二级菜单信息',
		               shadeClose: true,
		               shade: 0.5,
		               closeBtn:'1',//右上角xx关闭
		               area: ['1000px','500px'],
		               content:'SystemModelTwo.html',
		               yes: function(index, layero){
			           //点击确认触发 iframe 内容中的按钮提交
			           var submit = layero.find('iframe').contents().find("#layuiadmin-app-form-submit");
			           submit.click();
			           //表格刷新
		          	   table.reload('systemModel');
			        },
		               success : function(layero, index){
				            var body = layui.layer.getChildFrame('body', index);
				            if(layEvent === 'system'){
				                // 取到弹出层里的元素，并把编辑的内容放进去
				                var oldid = obj.data.sysid;
								sessionStorage.setItem('sysid',oldid);
								console.log(oldid);
				                body.find("#oldid").val(obj.data.sysid);  //将选中的数据的id传到编辑页面的隐藏域，便于根据ID修改数据
				                // 记得重新渲染表单
		                		form.render();
				            }
				            setTimeout(function(){
				                layui.layer.tips('点击此处返回用户列表', '.layui-layer-setwin .layui-layer-close', {
				                    tips: 3
				                });
				            },100)
				        }
		              
	             });
	              //同步更新缓存对应的值
		    obj.update({
		      username: '123'
		      ,title: 'xxx'
		    });
		  }
		  else if(layEvent === 'edit'){ //编辑
		    //do something
		   //脚本编辑弹出层
	             var name = encodeURIComponent(data.toolName);
	             //alert(name);
	             layer.open({
		               type: 2,
		               title: '修改信息',
		               shadeClose: true,
		               shade: 0.5,
		               closeBtn:'1',//右上角xx关闭
		               area: ['400px','500px'],
		               content:'editSystemModel.html',
		               btn: ['确定', '取消'],
		               yes: function(index, layero){
			           var submit = layero.find('iframe').contents().find("#btn_update");
			           submit.click();
			           //表格刷新
			        },
		               success : function(layero, index){
				            var body = layui.layer.getChildFrame('body', index);
				            if(layEvent === 'edit'){
				                // 取到弹出层里的元素，并把编辑的内容放进去
				                body.find("#sysid").val(obj.data.sysid);  //将选中的数据的id传到编辑页面的隐藏域，便于根据ID修改数据
				                body.find("#sysname").val(obj.data.sysname); 
				                body.find("#chinesename").val(obj.data.chinesename);
				                body.find("#imageurl").val(obj.data.imageurl);
				                body.find("#navurl").val(obj.data.navurl);
				                // 记得重新渲染表单
		                		form.render();
				            }
				            setTimeout(function(){
				                layui.layer.tips('点击此处返回用户列表', '.layui-layer-setwin .layui-layer-close', {
				                    tips: 3
				                });
				            },100)
				        }
	             });
	              //同步更新缓存对应的值
		    obj.update({
		      username: '123'
		      ,title: 'xxx'
		    });
		  }
		});
		$("#btn_addsystem").click(function(){
					var name = $("#name").val().trim();
					var chinesename = $("#chinesename").val().trim();
					var imageurl = $("#imageurl").val().trim();	
					//var navurl = $("#navurl").val().trim();
						if(name == "") {
							layer.tips('不能为空', '#name');
							return;
						} 
						if(chinesename==""){
							layer.tips('不能为空', '#chinesename');
							return;
						}
						if(imageurl==""){
							layer.tips('不能为空', '#imageurl');
							return;
						}
						
							//构建参数 
						var obj = {
							"name":name,
							"chinesename":chinesename,
							"imageurl":imageurl,
						};
						$.ajax({
								type : "POST",
								url : urlapi+'systemmodel/addsystemmodel',
								data : obj, //直接传对象参数 
								dataType : 'json',
								success : function(data) {
									if(data.code == 0){
										layer.msg(data.msg,{icon:6}); 
									}else{
										layer.msg(data.msg,{icon:5});
									}
								},
								error:function(){
									layer.msg("添加失败",{icon:5}); 
								}  
							});table.reload('systemModel');
						});
	  });