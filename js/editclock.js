layui.use(['table','jquery','form','layer'], function(){
  var table = layui.table;
  var $ = layui.jquery;
  var form=layui.form;
  var layer = layui.layer;
  
  queryclocklist();
  //查询用户下拉框
  function queryclocklist(){
  	$.ajax({
  		url: urlapi+'QrCode/getAllQRcodeList',
  		type: 'get',
  		xhrFields: {
  		 withCredentials: true
  		},
  		crossDomain: true,
  		success: function(data) {
  			//console.log(roledata);
  			if(data.code == 0) {
  				var str = "";
  				for(var i = 0; i < data.data.length; i++) {
  					str += '<option value=' + data.data[i].qrcodeid + '>' + data.data[i].contents + '</option>';
  				}
  				 $("#qrcodeid").append(str);
  				form.render();
  			} else {
  					layer.msg("打卡信息获取失败");
  			}
  		},
  		error: function() {
  			layer.msg("打卡信息获取失败");
  		}
  	});
  }
  
    //监听提交,添加事件
    form.on('submit(btn_update)', function(data){
      var field = data.field; //获取提交的字段
      var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引  
			var clockid=$("#clockid").val();
	    	var clocknote=$("#clocknote").val();
			var qrcodeid=$("#qrcodeid").val();
			var param = "?clockid=" + clockid + "&clocknote=" +clocknote + "&qrcodeid=" +qrcodeid;
			//alert(param)
			$.ajax({
				type:"get",
				url:urlapi+"clock/modifyclock" + param,
				xhrFields: {
					withCredentials: true
				},
				crossDomain: true,
				data:null,
				dataType : "json",
				success:function(date){
					if(date.code == 0){
						layer.msg(date.msg,{icon:6}); 
					}else{
						layer.msg(date.msg,{icon:5});
					}
					setTimeout(function(){
						parent.layui.table.reload('LAY-app-content-list'); //重载表格
     					parent.layer.close(index); //再执行关闭 
						parent.layui.table.reload('ClockModel'); //重载表格
					},1*1000)
				},
				error:function(date){
					layer.msg(date.msg,{icon:5});
				}
			})
      	return false;
    });
});