layui.use([ 'table', 'form', 'layer',  'laytpl', 'element' ], function() {
			var table = layui.table, form = layui.form, 
				layer = layui.layer, $ = layui.jquery,
				element = layui.element;
				
			/*加载表格*/
			table.render({
				elem : '#Rolemodel',
				id:'Rolemodel',
				url : urlapi+'blog/getbloglistnum',
				title : '',
				height : "full-160",
				skin : 'line',
				curr:1,
				even : true,
				cols : [ 
				     [{
						field : 'id',
						align : 'center',
						title : '编号',
						
					},
				     {
						field : 'blogid',
						align : 'center',
						title : '微博ID',
						
					}, 
					{
						field : 'releasetime',
						align : 'center',
						title : '发布时间',
							
					},
					{
						field : 'contents',
						align : 'center',
						title : '内容',
							
					},
					{
						field : 'blognote',
						align : 'center',
						title : '备注',
							
					},{
						field : 'userid',
						align : 'center',
						title : '用户ID',
						
					},{
						field : 'username',
						align : 'center',
						title : '用户名',
						
					},
					 {
						title: '操作',
						toolbar: '#barDemo',
						align: 'center',
						width : 180
						}] 
				 ],
				page: {
						layout: ['prev', 'page', 'next', 'skip', 'count', 'limit'],
						groups: 5,
						limit: 10,
						limits: [10, 20, 30, 40, 50],
						theme: '#1E9FFF',						
				 }
			});
			
			//条件查询
			$("#btnselbackrole").click(function(){
	   		var departmentselecet=$("#departmentselecet").val();
			var departmentselecet2=$("#departmentselecet2").val();
			var departmentselecet3=$("#departmentselecet3").val();
	   		var param = '?departmentselecet='+departmentselecet + '&departmentselecet2=' + departmentselecet2 +'&departmentselecet3='+departmentselecet3;
	   		table.render({
				elem : '#Rolemodel',
				id:'Rolemodel',
				url : urlapi+'blog/getbloglistnum' + param,
				title : '',
				height : "full-160",
				skin : 'line',
				curr:1,
				even : true,
				cols : [
				     [{
						field : 'id',
						align : 'center',
						title : '编号',
						
					},
				     {
						field : 'blogid',
						align : 'center',
						title : '微博ID',
						
					}, 
					{
						field : 'releasetime',
						align : 'center',
						title : '发布时间',
							
					},
					{
						field : 'contents',
						align : 'center',
						title : '内容',
							
					},
					{
						field : 'blognote',
						align : 'center',
						title : '备注',
							
					},{
						field : 'userid',
						align : 'center',
						title : '用户ID',
						
					},{
						field : 'username',
						align : 'center',
						title : '用户名',
						
					},
					 {
						title: '操作',
						toolbar: '#barDemo',
						align: 'center',
						width : 180
						}] 
				 ],
				page: {
						layout: ['prev', 'page', 'next', 'skip', 'count', 'limit'],
						groups: 5,
						limit: 10,
						limits: [10, 20, 30, 40, 50],
						theme: '#1E9FFF',						
				 }
			});
	   });
			
		table.on('tool(Rolemodel)', function(obj){ //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
			var data = obj.data; //获得当前行数据
			var layEvent = obj.event;
			//获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
			var tr = obj.tr; //获得当前行 tr 的DOM对象
			if(layEvent == 'del'){ //删除
				layer.confirm('真的删除行么', function(index){
			    obj.del(); //删除对应行（tr）的DOM结构，并更新缓存
			    layer.close(index);
					//向服务端发送删除指令
			      	$.post(urlapi+"blog/deleteblog",{id:obj.data.id},function(flag){
		    			if(flag){
		    				layer.msg("刪除成功",{icon:6}); //表格刷新
					        table.reload('Blogmodel'); //重载表格	
		    			}else{
		    				layer.msg("刪除失敗",{icon:5});
		    			}
			    	});
			    });
			} 
		})
})