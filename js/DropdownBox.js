layui.use([ 'table', 'form', 'layer',  'laytpl', 'element','laydate' ], function() {
	var table = layui.table, form = layui.form, 
		layer = layui.layer, $ = layui.jquery,
		element = layui.element,laydate = layui.laydate;
	
	queryuserlist();
	queryListCar();
	queryroutelist();
	
	queryadduserlist();
	queryaddcarlist();
	queryaddroutelist();
	
	//查询用户下拉框
	function queryuserlist(){
		$.ajax({
			url: urlapi+'user/getDriverList',
			type: 'get',
			xhrFields: {
			 withCredentials: true
			},
			crossDomain: true,
			success: function(data) {
				//console.log(roledata);
				if(data.code == 0) {
					var str = "";
					for(var i = 0; i < data.data.length; i++) {
						str += '<option value=' + data.data[i].userid + '>' + data.data[i].userid + '</option>';
					}
					 $("#userid").append(str);
					form.render();
				} else {
						layer.msg("用户信息获取失败");
				}
			},
			error: function() {
				layer.msg("用户信息获取失败");
			}
		});
	}
	
	//查询所有车辆信息下拉框
	function queryListCar(){
		$.ajax({
			url: urlapi+'car/getListCar',
			type: 'get',
			xhrFields: {
			 withCredentials: true
			},
			crossDomain: true,
			success: function(data) {
				//console.log(roledata);
				if(data.code == 0) {
					var str = "";
					for(var i = 0; i < data.data.length; i++) {
						str += '<option value=' + data.data[i].carid + '>' + data.data[i].carid + '</option>';
					}
					 $("#carid").append(str);
					form.render();
				} else {
						layer.msg("车辆信息获取失败");
				}
			},
			error: function() {
				layer.msg("车辆信息获取失败");
			}
		});
	}
	
	//查询未完成的线路下拉框
	function queryroutelist(){
		$.ajax({
			url: urlapi+'route/getListRoute',
			type: 'get',
			xhrFields: {
			 withCredentials: true
			},
			crossDomain: true,
			success: function(data) {
				//console.log(roledata);
				if(data.code == 0) {
					var str = "";
					for(var i = 0; i < data.data.length; i++) {
						str += '<option value=' + data.data[i].routeid + '>' + data.data[i].routename + '</option>';
					}
					 $("#routeid").append(str);
					form.render();
				} else {
						layer.msg("路线信息获取失败");
				}
			},
			error: function() {
				layer.msg("路线信息获取失败");
			}
		});
	}
	
	//查询用户下拉框
	function queryadduserlist(){
		$.ajax({
			url: urlapi+'user/getDriverList',
			type: 'get',
			xhrFields: {
			 withCredentials: true
			},
			crossDomain: true,
			success: function(data) {
				//console.log(roledata);
				if(data.code == 0) {
					var str = "";
					for(var i = 0; i < data.data.length; i++) {
						str += '<option value=' + data.data[i].userid + '>' + data.data[i].userid + '</option>';
					}
					 $("#adduserid").append(str);
					form.render();
				} else {
						layer.msg("用户信息获取失败");
				}
			},
			error: function() {
				layer.msg("用户信息获取失败");
			}
		});
	}
	
	//查询投入使用的车辆下拉框
	function queryaddcarlist(){
		$.ajax({
			url: urlapi+'car/getListCar',
			type: 'get',
			xhrFields: {
			 withCredentials: true
			},
			crossDomain: true,
			success: function(data) {
				//console.log(roledata);
				if(data.code == 0) {
					var str = "";
					for(var i = 0; i < data.data.length; i++) {
						str += '<option value=' + data.data[i].carid + '>' + data.data[i].carid + '</option>';
					}
					 $("#addcarid").append(str);
					form.render();
				} else {
						layer.msg("车辆信息获取失败");
				}
			},
			error: function() {
				layer.msg("车辆信息获取失败");
			}
		});
	}
	
	//查询未完成的线路下拉框
	function queryaddroutelist(){
		$.ajax({
			url: urlapi+'route/getListRoute',
			type: 'get',
			xhrFields: {
			 withCredentials: true
			},
			crossDomain: true,
			success: function(data) {
				//console.log(roledata);
				if(data.code == 0) {
					var str = "";
					for(var i = 0; i < data.data.length; i++) {
						str += '<option value=' + data.data[i].routeid + '>' + data.data[i].routename + '</option>';
					}
					 $("#addrouteid").append(str);
					form.render();
				} else {
						layer.msg("路线信息获取失败");
				}
			},
			error: function() {
				layer.msg("路线信息获取失败");
			}
		});
	}
	
});