//查询开始
layui.use([ 'table', 'form', 'layer',  'laytpl', 'element','laydate' ], function() {
	var table = layui.table, form = layui.form, 
		layer = layui.layer, $ = layui.jquery,
		element = layui.element,laydate = layui.laydate;
	
	laydate.render({
	  elem: '#createtime' //指定元素
	});
	/* 页面打开数据加载 */
	querylist();
		
	/* 点击查询对网站用户进行筛选 */
	$("#btnselbackdepart").click(function(){
		querylist();//调用局部刷新
	});
		
	function querylist(){
		/* 页面打开数据加载 */
		var codeid=$("#codeid").val();
		var param = "?qrcodeid="+codeid;
		//alert(param)
		/*加载表格*/
		table.render({
			elem : '#QrCodelist',
			id:'QrCodelist',
			url : urlapi+'QrCode/getqrcodelist'+param,
			title : '',
			height : "full-160",
			skin : 'line',
			curr:1,
			even : true,
			cols: [
			[ 
			{
				field: 'qrcodeid',
				align: 'center',
				title: '编号'						
			} ,
			{
				field: 'qrcodetime',
				align: 'center',
				title: '创建时间'						
			} ,
			{
				field: 'contents',
				align: 'center',
				title: '为本内容'						
			} ,
			{
				field: 'qrcodenote',
				align: 'center',
				title: '备注'						
			},{
				title: '操作',
				toolbar: '#barDemo',
				align: 'center',
				width : 230
			}] 
			 ],
			page: {
				layout: ['prev', 'page', 'next', 'skip', 'count', 'limit'],
				groups: 5,
				limit: 10,
				limits: [10, 20, 30, 40, 50],
				theme: '#1E9FFF',						
			 },
		});
	}
	/*点击查询加载表格数据结束*/
	
	table.on('tool(QrCodelist)', function(obj){ //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
	    var data = obj.data; //获得当前行数据
	    var layEvent = obj.event;
	    //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
	    var tr = obj.tr; //获得当前行 tr 的DOM对象
	    if(layEvent == 'del'){ //删除
			layer.confirm('真的删除行么', function(index){
				obj.del(); //删除对应行（tr）的DOM结构，并更新缓存
				layer.close(index);
				//向服务端发送删除指令
				$.post(urlapi+"QrCode/deleteqrcode",{qrcodeid:obj.data.qrcodeid},function(flag){
					if(flag){
						layer.msg("刪除成功",{icon:6}); //表格刷新
						table.reload('Userlist'); //重载表格	
					}else{
						layer.msg("刪除失敗",{icon:5});
					}
				});
			});
	   } else if(layEvent == 'edit'){ //编辑
		//do something
	   //脚本编辑弹出层
		var name = encodeURIComponent(data.toolName);
		//alert(name);
		layer.open({
		    type: 2,
		    title: '修改信息',
		    shadeClose: true,
		    shade: 0.5,
		    closeBtn:'1',//右上角xx关闭
		    area: ['500px','300px'],
		    content:'edit_QRcode.html',
		    btn: ['确定', '取消'],
		    yes: function(index, layero){
		    //点击确认触发 iframe 内容中的按钮提交
		    var submit = layero.find('iframe').contents().find("#btn_update");
		    submit.click();
		    },
		    success : function(layero, index){
				var body = layui.layer.getChildFrame('body', index);
				if(layEvent == 'edit'){
					// 取到弹出层里的元素，并把编辑的内容放进去
					body.find("#qrcodeid").val(obj.data.qrcodeid);  //将选中的数据的id传到编辑页面的隐藏域，便于根据ID修改数据
					body.find("#contents").val(obj.data.contents); 
					body.find("#qrcodenote").val(obj.data.qrcodenote);
					// 记得重新渲染表单
					form.render();
				}
				setTimeout(function(){
					layui.layer.tips('点击此处返回用户列表', '.layui-layer-setwin .layui-layer-close', {
						tips: 3
					});
				},100)
			}
		 });
    }else if(layEvent == 'qrcode'){
		  var name = encodeURIComponent(data.toolName);
		  layer.open({
		      type: 2,
		      title: '生成二维码信息',
		      shadeClose: true,
		      shade: 0.5,
		      closeBtn:'1',//右上角xx关闭
		      area: ['550px','400px'],
		      content:'GenerateQrCode.html',
		      btn: ['确定', '取消'],
		      yes: function(index, layero){
		      //点击确认触发 iframe 内容中的按钮提交
		      var submit = layero.find('iframe').contents().find("#send");
		      submit.click();
		      },
		      success : function(layero, index){
		  		var body = layui.layer.getChildFrame('body', index);
		  		if(layEvent == 'qrcode'){
		  			// 取到弹出层里的元素，并把编辑的内容放进去
		  			//body.find("#qrcodeid").val(obj.data.qrcodeid);  //将选中的数据的id传到编辑页面的隐藏域，便于根据ID修改数据
		  			body.find("#contents").val("编号:"+obj.data.qrcodeid+"打卡内容:"+obj.data.contents);
		  			// 记得重新渲染表单
		  			form.render();
		  		}
		  		setTimeout(function(){
		  			layui.layer.tips('点击此处返回用户列表', '.layui-layer-setwin .layui-layer-close', {
		  				tips: 3
		  			});
		  		},100)
		  	}
		   });
	  }
	});		
    //查询结束
	
	//添加一条方法开始
	$("#btn_addQrCode").click(function(){
		//获取
		var qrcodeid=$("#qrcodeid").val();
		var contents=$("#contents").val();
		var qrcodenote=$("#qrcodenote").val();
		
		//构建参数 
		var obj = {
			"qrcodeid":qrcodeid,
			"contents":contents,
			"qrcodenote":qrcodenote
			
		}
		$.ajax({
			type : "POST",
			url : urlapi+'QrCode/addqrcode',
			data : obj, //直接传对象参数 
			dataType : 'json',
			success : function(data) {
				layer.msg(data.msg,{icon:6}); //表格刷新
				table.reload('Userlist'); //重载表格	
			},
			error:function(){
				layer.msg(data.msg,{icon:5}); //表格刷新;
			}  
		});
		return false;
    });
	//添加结束
});