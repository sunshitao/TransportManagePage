//查询开始
layui.use([ 'table', 'form', 'layer',  'laytpl', 'element','laydate' ], function() {
	var table = layui.table, form = layui.form, 
		layer = layui.layer, $ = layui.jquery,
		element = layui.element,laydate = layui.laydate;
	
	 //日期(添加用户部分)
	  laydate.render({
	    elem: '#createtime'
	  });
	
	/* 页面打开数据加载 */
	querylist();
		
	/* 点击查询对网站用户进行筛选 */
	$("#btnselbackdepart").click(function(){
		querylist();//调用局部刷新
	});
		
	function querylist(){
		/* 页面打开数据加载 */
		var userid=$("#userid").val();
		var carid=$("#carid").val();
		var tasknote=$("#tasknote").val();
		if(tasknote == 0){
			var tasknote = "";
		}else if(tasknote == 1){
			var tasknote = "已完成";
		}else if(tasknote == 2){
			var tasknote = "未完成";
		}
		var routeid=$("#routeid").val();
		var param = "?userid="+userid+"&carid="+carid+"&tasknote="+tasknote+"&routeid="+routeid;
		//alert(param)
		/*加载表格*/
		table.render({
			elem : '#table',
			id:'table',
			url : urlapi+'task/gettasklist'+param,
			title : '',
			height : "full-160",
			skin : 'line',
			curr:1,
			even : true,
			cols: [
			[ 
			{
				field: 'taskid',
				align: 'center',
				title: '任务编号'		,
				hide : true,
			} ,
			{
				field: 'userid',
				align: 'center',
				title: '司机编号'						
			} ,
			{
				field: 'carid',
				align: 'center',
				title: '运输车辆'						
			} ,
			{
				field: 'createdate',
				align: 'center',
				title: '创建时间'						
			} ,
			{
				field: 'routename',
				align: 'center',
				title: '线路名称'						
			},
			{
				field: 'endIsdit',
				align: 'center',
				title: '目的地打卡'						
			},
			{
				field: 'tasknote',
				align: 'center',
				title: '工单状态'						
			},
			 {
				title: '操作',
				toolbar: '#barDemo',
				align: 'center',
				width : 250
			}] 
			 ],
			page: {
				layout: ['prev', 'page', 'next', 'skip', 'count', 'limit'],
				groups: 5,
				limit: 10,
				limits: [10, 20, 30, 40, 50],
				theme: '#1E9FFF',						
			 },
		});
	}
	/*点击查询加载表格数据结束*/
	
	table.on('tool(table)', function(obj){ //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
	    var data = obj.data; //获得当前行数据
	    var layEvent = obj.event;
	    //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
	    var tr = obj.tr; //获得当前行 tr 的DOM对象
	    if(layEvent == 'del'){ //删除
			layer.confirm('真的删除行么', function(index){
				obj.del(); //删除对应行（tr）的DOM结构，并更新缓存
				layer.close(index);
				//向服务端发送删除指令
				$.post(urlapi+"task/deletetask",{taskid:obj.data.taskid},function(flag){
					if(flag){
						layer.msg(flag.msg,{icon:6}); //表格刷新
						table.reload('table'); //重载表格	
					}else{
						layer.msg(flag.msg,{icon:5});
					}
				});
			});
	   } else if(layEvent == 'edit'){ //编辑
		//do something
	   //脚本编辑弹出层
		var name = encodeURIComponent(data.toolName);
		//alert(name);
		layer.open({
		    type: 2,
		    title: '修改信息',
		    shadeClose: true,
		    shade: 0.5,
		    closeBtn:'1',//右上角xx关闭
		    area: ['500px','350px'],
		    content:'edit_Task.html',
		    btn: ['确定', '取消'],
		    yes: function(index, layero){
		    //点击确认触发 iframe 内容中的按钮提交
		    var submit = layero.find('iframe').contents().find("#btn_update");
		    submit.click();
			},
		    success : function(layero, index){
				var body = layui.layer.getChildFrame('body', index);
				if(layEvent == 'edit'){
					// 取到弹出层里的元素，并把编辑的内容放进去
					body.find("#taskid").val(obj.data.taskid);//将选中的数据的id传到编辑页面的隐藏域，便于根据ID修改数据
					body.find("#userid").val(obj.data.userid);  
					body.find("#carid").val(obj.data.carid); 
					body.find("#routeid").val(obj.data.routeid);
					body.find("#tasknote").val(obj.data.tasknote);
					// 记得重新渲染表单
					form.render();
				}
				setTimeout(function(){
					layui.layer.tips('点击此处返回用户列表', '.layui-layer-setwin .layui-layer-close', {
						tips: 3
					});
				},100)
			}
		 });
		//同步更新缓存对应的值
		obj.update({
		  username: '123'
		  ,title: 'xxx'
		});
	  }
	  else if(layEvent == 'system'){ 
	  		//do something
	   //脚本编辑弹出层
	  		var name = encodeURIComponent(data.toolName);
	  		//alert(name);
	  		layer.open({
	  		    type: 2,
	  		    title: '添加阀值信息',
	  		    shadeClose: true,
	  		    shade: 0.5,
	  		    closeBtn:'1',//右上角xx关闭
	  		    area: ['1000px','500px'],
	  		    content:'addmaxtemp.html',
	  		    btn: ['确定', '取消'],
	  		    yes: function(index, layero){
	  		       //点击确认触发 iframe 内容中的按钮提交
	  		       var submit = layero.find('iframe').contents().find("#layuiadmin-app-form-submit");
	  		       submit.click();
	  		       //表格刷新
	  		       table.reload('table');
	  		    },
	  		   success : function(layero, index){
	  		        var body = layui.layer.getChildFrame('body', index);
	  		        if(layEvent === 'system'){
	  		            // 取到弹出层里的元素，并把编辑的内容放进去
	  		            var oldid = obj.data.taskid;
	  		   								sessionStorage.setItem('taskid',oldid);
	  		   								console.log(oldid);
	  		            body.find("#oldid").val(obj.data.taskid);  //将选中的数据的id传到编辑页面的隐藏域，便于根据ID修改数据
	  		            // 记得重新渲染表单
	  		    		form.render();
	  		        }
	  		        setTimeout(function(){
	  		            layui.layer.tips('点击此处返回用户列表', '.layui-layer-setwin .layui-layer-close', {
	  		                tips: 3
	  		            });
	  		        },100)
	  		    }
	  		 });
	  		//同步更新缓存对应的值
	  		obj.update({
	  		  username: '123'
	  		  ,title: 'xxx'
	  		});
	  }
	});		
    //查询结束
	
	//添加一条方法开始
	$("#btn_addTask").click(function(){
		//获取
		var userid=$("#adduserid").val();
		var carid=$("#addcarid").val();
		var routeid=$("#addrouteid").val();
		//构建参数 
		var obj = {
			"userid":userid,
			"carid":carid,
			"routeid":routeid,
		}
		$.ajax({
			type : "POST",
			url : urlapi+'task/addtask',
			data : obj, //直接传对象参数 
			dataType : 'json',
			success : function(data) {
				layer.msg(data.msg,{icon:6}); //表格刷新
				table.reload('table'); //重载表格	
			},
			error:function(){
				layer.msg(data.msg,{icon:5}); //表格刷新;
			},
		});
		return false;
		//return true;
    });
	//添加结束
});