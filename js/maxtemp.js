layui.use([ 'table', 'form', 'layer', 'laydate', 'laytpl', 'element' ], function() {
		var table = layui.table, form = layui.form, 
			layer = layui.layer, $ = layui.jquery,
			laydate = layui.laydate, laytpl = layui.laytpl,
			element = layui.element;
		var oldid = $("#oldid").val().trim();
		console.log(oldid);
		
		var param ="?taskid="+oldid;
		/*加载表格*/
		table.render({
			elem : '#maxtemplist',
			id:'maxtemplist',
			url : urlapi+'TempValve/getTempValvelist' + param,
			title : '阀值数据表',
			height: "full-130",
			skin : 'line',
			even : true,
			cols : [ [ 
				{
					field : 'tmpevalveid',
					align : 'center',
					title : 'ID',
					width : 100,
				},{
					field : 'taskid',
					align : 'center',
					title : '任务编号',
					width : 100,
				}, {
					field : 'maxtemp',
					align : 'center',
					title : '最高温度',
					width : 100,
				}, {
					field : 'mintemp',
					align : 'center',
					title : '最低温度',
					width : 100,
				}, {
					field : 'temptime',
					align : 'center',
					title : '创建时间',
					width : 200,
				},
				{
					field : 'temnote',
					align : 'center',
					title : '备注',
					width : 200,
				},{
					title : '操作',
					toolbar : '#barDemo',
					align : 'center',
					width : 220,
				} ] 
			 ],
			 page: {
					layout: ['prev', 'page', 'next', 'skip', 'count', 'limit'],
					groups: 5,
					limit: 10,
					limits: [10, 20, 30, 40, 50],
					theme: '#1E9FFF',						
			 },
		});
		//table监听工具条
		table.on('tool(maxtemplist)', function(obj){ //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
		  var data = obj.data; //获得当前行数据
		  var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
		  var tr = obj.tr; //获得当前行 tr 的DOM对象
		 
		  if(layEvent === 'delete'){ //删除
		    layer.confirm('真的删除行么', function(index){
		      obj.del(); //删除对应行（tr）的DOM结构，并更新缓存
		      layer.close(index);
		      //向服务端发送删除指令
		      	$.post(urlapi+"TempValve/deleteTempValve",{tmpevalveid:obj.data.tmpevalveid},function(flag){
	    			if(flag){
	    				layer.msg(flag.msg,{icon:6}); //表格刷新
				        table.reload('maxtemplist'); //重载表格	
	    			}else{
	    				layer.msg(flag.msg,{icon:5});
	    			}
		    	});
		    });
		  } else if(layEvent === 'edit'){ //编辑
		    //do something
		   //脚本编辑弹出层
	             var name = encodeURIComponent(data.toolName);
	             //alert(name);
	             layer.open({
		               type: 2,
		               title: '修改阀值',
		               shadeClose: true,
		               shade: 0.5,
		               closeBtn:'1',//右上角xx关闭
		               area: ['400px','400px'],
		               content:'editmaxtemp.html',
		               btn: ['确定', '取消'],
		               yes: function(index, layero){
			           //点击确认触发 iframe 内容中的按钮提交
			           var submit = layero.find('iframe').contents().find("#btn_update");
			           submit.click();
			        },
		               success : function(layero, index){
				            var body = layui.layer.getChildFrame('body', index);
				            if(layEvent === 'edit'){
				                // 取到弹出层里的元素，并把编辑的内容放进去
				                body.find("#tmpevalveid").val(obj.data.tmpevalveid);  //将选中的数据的id传到编辑页面的隐藏域，便于根据ID修改数据
				                body.find("#maxtemp").val(obj.data.maxtemp); 
				                body.find("#mintemp").val(obj.data.mintemp);
								body.find("#taskid").val(obj.data.taskid);
								body.find("#temnote").val(obj.data.temnote);
				                // 记得重新渲染表单
		                		form.render();
				            }
				            setTimeout(function(){
				                layui.layer.tips('点击此处返回用户列表', '.layui-layer-setwin .layui-layer-close', {
				                    tips: 3
				                });
				            },100)
				        }
		              
	             });
	              //同步更新缓存对应的值
		    obj.update({
		      username: '123'
		      ,title: 'xxx'
		    });
		  }
	  });
	  /*加载表格结束*/
		$("#addSystemModelTow").click(function(){
			//调用方法加载select管理员角色
			layer.open({
				type : 1,
				title : '添加',
				area : [ '400px', '400px' ],
				shade : 0.4,
				content : $('#div_addmajor'),
				btn : [ '保存', '返回' ],
				yes : function() {
					//获取
					var taskid=$("#oldid").val();
					var maxtemp=$("#maxtemp").val();
					var mintemp=$("#mintemp").val();	
					var temnote=$("#temnote").val();	
					//构建参数 
					var obj = {
						"taskid":taskid,
						"maxtemp":maxtemp,
						"mintemp":mintemp,
						"temnote":temnote,
					}
					//通过ajax传值，传到后台做数据交换
					$.ajax({
						url : urlapi+"TempValve/getaddTempValve",
						type : "POST",
						data : obj, //直接传对象参数 
						dataType : 'json',
						success : function(data) {
							if(data.code == 0){
								layer.msg(data.msg,{icon:6}); 
								table.reload('maxtemplist');
								
							}else{
								layer.msg(data.msg,{icon:5});
							}
							setTimeout(function(){
     							parent.layer.close(index); //再执行关闭 
								parent.layui.table.reload('maxtemplist'); //重载表格
							},1*1000)
						},
						error:function(data){
							layer.msg(data.msg,{icon:5});
						},
						
					})
				},
		});	
		return false;
		});
		//添加结束
	})