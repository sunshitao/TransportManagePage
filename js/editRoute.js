layui.use([ 'table', 'form', 'layer',  'laytpl', 'element','laydate' ], function() {
	var table = layui.table, form = layui.form, 
		layer = layui.layer, $ = layui.jquery,
		element = layui.element,laydate = layui.laydate;
	
	//出发时间
    laydate.render({
		elem: '#statetime'
    });
	//到达时间
    laydate.render({
		elem: '#endtime'
    });
		  
	//监听提交,添加事件
	form.on('submit(btn_update)', function(data){
	  var field = data.field; //获取提交的字段
	  var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引  
			var routeid=$("#routeid").val().trim();
			var routename=$("#routename").val().trim();
			var start=$("#start").val();
			var ends=$("#ends").val();
			var statetime=$("#statetime").val();
			var endtime=$("#endtime").val();
			var endIsdit=$("#endIsdit").val();
			var routenote=$("#routenote").val();
			//alert(routeid);
			var obj = {
						"routeid":routeid,
						"routename":routename,
						"start":start,
						"ends":ends,	
						"statetime":statetime,		
						"endtime":endtime,
						"endIsdit":endIsdit,
						"routenote":routenote,
						};
						
			$.ajax({
				url: urlapi+'route/modifyroute',
				type:"POST",
				data: obj,
				dataType : "json",
				success : function(data) {
					if(data.code == 0){
						layer.msg(data.msg,{icon:6}); 
						table.reload('table');
						
					}else{
						layer.msg(data.msg,{icon:5});
					}
					setTimeout(function(){
						parent.layer.close(index); //再执行关闭 
						parent.layui.table.reload('table'); //重载表格
					},1*1000)
				},
				error:function(data){
					layer.msg(data.msg,{icon:5});
				},
			})
	});
	return false;
});