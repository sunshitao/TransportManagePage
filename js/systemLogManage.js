layui.use([ 'table', 'form', 'layer',  'laytpl', 'element','layedit', 'laydate' ], function() {
		var table = layui.table, form = layui.form, 
			layer = layui.layer, $ = layui.jquery,
			element = layui.element;
			layedit = layui.layedit;
  			laydate = layui.laydate;
		 
		//日期
		laydate.render({
			elem: '#date'
			,type: 'datetime'
		});
		laydate.render({
			elem: '#date2'
			,type: 'datetime'
		});
	
	function localFormatter(value) {
        if (value.trim() == "add") {
            return "<span class='label'  style='color:#993366'>添加</span>";
        } else if (value.trim() == "modify") {
            return "<span class='label'  style='color:#993366'>修改</span>";
        } else if (value.trim() == "delete") {
            return "<span class='label'  style='color:#993366'>删除</span>";
        }else if (value.trim() == "query") {
            return "<span class='label'  style='color:#993366'>查询</span>";
        }
    }
	
	/*加载表格*/
	table.render({
		elem : '#blogUser',
		id:'blogUserid',
		url : urlapi+'systemlog/getsystemloglist',
		title : '后台用户数据表',
		height: "full-160",
		skin : 'line',
		even : true,
		cols : [
				[ {
					field : 'id',
					align : 'center',
					title : '编号',
				}, {
					field : 'opertype',
					align : 'center',
					title : '日志类型',
					width : 150,
					templet:function(d){
					return localFormatter(d.opertype)}
				}, {
					field : 'description',
					title : '描述',
					align : 'center'
				}, {
					field : 'opermethod',
					align : 'center',
					title : '系统方法'
				}, {
					field : 'userid',
					align : 'center',
					title : '用户',
				}, {
					field : 'params',
					align : 'center',
					title : '参数',
				}, {
					field : 'createdate',
					align : 'center',
					title : '创建时间',
					width : 200,
					templet : "<div>{{layui.util.toDateString(d.createdate, 'yyyy-MM-dd HH:mm:ss')}}</div>"
				}]
			],
		 page: {
				layout: ['prev', 'page', 'next', 'skip', 'count', 'limit'],
				groups: 5,
				limit: 10,
				limits: [10, 20, 30, 40, 50],
				theme: '#1E9FFF',						
		 },
	});
		
	/* 点击查询对网站用户进行筛选 */
	$("#btnselfrontinfo").click(function(){
		var logtype=$("#logtype").val().trim();
		var opermethod=$("#opermethod").val().trim();
		var userid=$("#userid").val().trim();
		var date=$("#date").val();

		//alert(logtype)
		prarm = '?logtype=' + logtype + '&opermethod=' + opermethod+ '&userid=' 
						+ userid + '&date=' + date;
		
		table.render({
			elem : '#blogUser',
			id:'blogUserid',
			url : urlapi+'systemlog/getsystemloglist' + prarm,
			title : '后台用户数据表',
			height: "full-160",
			skin : 'line',
			even : true,
			cols : [
				[ {
					field : 'id',
					align : 'center',
					title : '编号',
				}, {
					field : 'opertype',
					align : 'center',
					title : '日志类型',
					width : 150,
					templet:function(d){
					return localFormatter(d.opertype)}
				}, {
					field : 'description',
					title : '描述',
					align : 'center'
				}, {
					field : 'opermethod',
					align : 'center',
					title : '系统方法'
				}, {
					field : 'userid',
					align : 'center',
					title : '用户',
				}, {
					field : 'params',
					align : 'center',
					title : '参数',
				}, {
					field : 'createdate',
					align : 'center',
					title : '创建时间',
					width : 200,
					templet : "<div>{{layui.util.toDateString(d.createdate, 'yyyy-MM-dd HH:mm:ss')}}</div>"
				},]
			],
			 page: {
					layout: ['prev', 'page', 'next', 'skip', 'count', 'limit'],
					groups: 5,
					limit: 10,
					limits: [10, 20, 30, 40, 50],
					theme: '#1E9FFF',						
			 },
		});
	});
	//结束
		
	//删除开始
	$("#deleteAllLog").click(function(){
		layer.confirm('真的删除行么', function(index){
			layer.close(index);
		  //向服务端发送删除指令
			$.post(urlapi+'systemlog/deleteAllLog',function(flag){
				if(flag){
					layer.msg("刪除成功",{icon:6}); 
					table.reload('blogUserid'); 
				}else{
					layer.msg("刪除失敗",{icon:5});
				}
				
			})
			
		})		
	})
})