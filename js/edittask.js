layui.use(['table','jquery','form','layer'], function(){
	var table = layui.table;
	var $ = layui.jquery;
	var form=layui.form;
	var layer = layui.layer;
	//监听提交,添加事件
	form.on('submit(btn_update)', function(data){
	  var field = data.field; //获取提交的字段
	  var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引  
			var taskid=$("#taskid").val();
			var oldcarid=$("#carid").val();
			var userid=$("#adduserid").val();
			var carid=$("#addcarid").val();
			var routeid=$("#addrouteid").val();
			var tasknote=$("#tasknote").val();
			if(tasknote == 0){
				var tasknote = "";
			}else if(tasknote == 1){
				var tasknote = "已完成";
			}else if(tasknote == 2){
				var tasknote = "未完成";
			}
			var param ="?taskid="+taskid+ "&oldcarid=" + oldcarid + "&userid=" +userid + "&carid=" +carid+ "&routeid=" +routeid+ "&tasknote=" +tasknote;
			//alert(param)
			$.ajax({
				type:"get",
				url:urlapi+'task/modifytask' + param,
				xhrFields: {
				 withCredentials: true
				},
				crossDomain: true,
				data:null,
				dataType : "json",
				success:function(data){
					if(data.code == 0){
						layer.msg(data.msg,{icon:6}); 
					}else{
						layer.msg(data.msg,{icon:5});
					}
					setTimeout(function(){
						parent.layui.table.reload('LAY-app-content-list'); //重载表格
						parent.layer.close(index); //再执行关闭 
						parent.layui.table.reload('table'); //重载表格
					},1*1000)
				},
				error:function(data){
					layer.msg(data.msg,{icon:5});
				}
			})
		return false;
	});
});