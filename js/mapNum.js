//高德地图接口
var Positions = [];
var start = [];
$.ajax({
	url: url = urlapi+'mapnum/getmapnumlist',
	type: "get",
	xhrFields: {
		withCredentials: true
	},
	async:false,//是否异步
	crossDomain: true,
	success: function (data) {
		console.log(data);
		for(var i=0; i<data.data.length; i++) {
			var lng = data.data[i].longitude;
			var lat = data.data[i].latitude;
			let x_pi = 3.14159265358979324 * 3000.0 / 180.0;
			let x = lng - 0.0065;
			let y = lat - 0.006;
			let z = Math.sqrt(x * x + y * y) - 0.00002 * Math.sin(y * x_pi);
			let theta = Math.atan2(y, x) - 0.000003 * Math.cos(x * x_pi);
			let lngs = z * Math.cos(theta);
			let lats = z * Math.sin(theta);
			
			Positions[i] = [];
			Positions[i][0] = lngs;
			Positions[i][1] = lats;
		}
		return Positions;
	},
	error: function (XMLHttpRequest, textStatus) { //请求失败
		alert("未知错误1，请联系管理员");
	}
});
start = Positions[0]
console.log("打印全局变量");
console.log(Positions);
console.log("测试1:"+Positions[0][0]);
console.log("测试2:"+Positions[0]);
console.log("打印中心点:"+start)