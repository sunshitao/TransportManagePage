layui.use([ 'table', 'form', 'layer', 'laydate', 'laytpl', 'element' ], function() {
		var table = layui.table, form = layui.form, 
			layer = layui.layer, $ = layui.jquery,
			laydate = layui.laydate, laytpl = layui.laytpl,
			element = layui.element;
		var oldid = $("#oldid").val().trim();
		console.log(oldid);
		
		var param ="?oldid="+oldid;
		 
		/*加载表格*/
		table.render({
			elem : '#SystemModelTow',
			id:'SystemModelTow',
			url : urlapi+'systemmodel/getsystemmodellisttwo' + param,
			title : '后台管理员用户数据表',
			height: "full-130",
			skin : 'line',
			even : true,
			cols : [ [ 
			     {
					field : 'sysid',
					title : 'ID',
					width : 50,
				}, {
					field : 'sysname',
					align : 'center',
					title : '菜单名称',
					width : 200,
				}, {
					field : 'chinesename',
					align : 'center',
					title : '展示名称',
					width : 200,
				}, {
					field : 'navurl',
					align : 'center',
					title : '跳转页面',
				},{
					title : '操作',
					toolbar : '#barDemo',
					align : 'center',
					width : 220,
				} ] 
			 ],
			 page: {
					layout: ['prev', 'page', 'next', 'skip', 'count', 'limit'],
					groups: 5,
					limit: 10,
					limits: [10, 20, 30, 40, 50],
					theme: '#1E9FFF',						
			 },
		});
		//table监听工具条
		table.on('tool(SystemModelTow)', function(obj){ //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
		  var data = obj.data; //获得当前行数据
		  var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
		  var tr = obj.tr; //获得当前行 tr 的DOM对象
		 
		  if(layEvent === 'delete'){ //删除
		    layer.confirm('真的删除行么', function(index){
		      obj.del(); //删除对应行（tr）的DOM结构，并更新缓存
		      layer.close(index);
		      //向服务端发送删除指令
		      	$.post(urlapi+"systemmodel/deletesystemmodel",{sysid:obj.data.sysid},function(flag){
	    			if(flag){
	    				layer.msg("刪除成功",{icon:6}); //表格刷新
				        table.reload('test'); //重载表格	
	    			}else{
	    				layer.msg("刪除失敗",{icon:5});
	    			}
		    	});
		    });
		  } else if(layEvent === 'edit'){ //编辑
		    //do something
		   //脚本编辑弹出层
	             var name = encodeURIComponent(data.toolName);
	             //alert(name);
	             layer.open({
		               type: 2,
		               title: '修改二级菜单',
		               shadeClose: true,
		               shade: 0.5,
		               closeBtn:'1',//右上角xx关闭
		               area: ['400px','400px'],
		               content:'editSystemModelTwo.html',
		               btn: ['确定', '取消'],
		               yes: function(index, layero){
			           //点击确认触发 iframe 内容中的按钮提交
			           var submit = layero.find('iframe').contents().find("#btn_update");
			           submit.click();
			        },
		               success : function(layero, index){
				            var body = layui.layer.getChildFrame('body', index);
				            if(layEvent === 'edit'){
				                // 取到弹出层里的元素，并把编辑的内容放进去
				                body.find("#id").val(obj.data.sysid);  //将选中的数据的id传到编辑页面的隐藏域，便于根据ID修改数据
				                body.find("#sysname").val(obj.data.sysname); 
				                body.find("#chinesename").val(obj.data.chinesename);
				                body.find("#imageurl").val(obj.data.imageurl);
				                body.find("#navurl").val(obj.data.navurl);
				                // 记得重新渲染表单
		                		form.render();
				            }
				            setTimeout(function(){
				                layui.layer.tips('点击此处返回用户列表', '.layui-layer-setwin .layui-layer-close', {
				                    tips: 3
				                });
				            },100)
				        }
		              
	             });
	              //同步更新缓存对应的值
		    obj.update({
		      username: '123'
		      ,title: 'xxx'
		    });
		  }
	  });
	  /*加载表格结束*/
		$("#addSystemModelTow").click(function(){
			//调用方法加载select管理员角色
			layer.open({
				type : 1,
				title : '添加',
				area : [ '400px', '400px' ],
				shade : 0.4,
				content : $('#div_addmajor'),
				btn : [ '保存', '返回' ],
				yes : function() {
					//获取
					var oldid=$("#oldid").val();
					var name=$("#name").val();
					var chinesename=$("#chinesename").val();	
					var navurl=$("#navurl").val();	
					//构建参数 
					var obj = {
						"id":oldid,
						"name":name,
						"chinesename":chinesename,
						"navurl":navurl,
					}
					//通过ajax传值，传到后台做数据交换
					$.ajax({
						url : urlapi+"systemmodel/addsystemmodeltwo",
						type : "POST",
						data : obj, //直接传对象参数 
						dataType : 'json',
						success : function(data) {
							if(data.code == 0){
								layer.msg(data.msg,{icon:6}); 
								table.reload('SystemModelTow');
								
							}else{
								layer.msg(data.msg,{icon:5});
							}
							setTimeout(function(){
     							parent.layer.close(index); //再执行关闭 
								parent.layui.table.reload('SystemModelTow'); //重载表格
							},1*1000)
						},
						error:function(data){
							layer.msg(data.msg,{icon:5});
						},
						
					})
				},
		});	
		return false;
		});
		//添加结束
	})