layui.use([ 'table', 'form', 'layer', 'laydate', 'laytpl', 'element' ], function() {
		var table = layui.table, form = layui.form, 
			layer = layui.layer, $ = layui.jquery,
			laydate = layui.laydate, laytpl = layui.laytpl,
			element = layui.element;
			
		//页面打开加载数据
		querylist();
		
		//点击查询，筛选菜单
		$("#btnselfrontinfo").click(function(){
			querylist();
		});
	
		function querylist(){
			var sysname=$("#sysname").val();
			var chinesename=$("#chinesename").val();
			var param = "?sysname="+sysname+"&chinesename="+chinesename;
			/*加载表格*/
			table.render({
				elem : '#powerinfo',
				id:'powerinfo',
				url : urlapi+'systemmodel/getallsystemmodel'+param,
				title : '后台管理员用户数据表',
				height: 345,
				skin : 'line',
				even : true,
				cols : [ 
				     [ 
				     {
						field : 'sysid',
						title : 'ID',
						hide: true ,
						width:160
					}, {
						field : 'sysname',
						align : 'center',
						title : '菜单名称',
						width : 170,
					}, {
						field : 'chinesename',
						align : 'center',
						title : '展示名称',
						width : 170,
					}, {
						field : 'deepth',
						title : '菜单层次',
						align : 'center',
						width : 170,templet:function(d){
				    		 return d.deepth == "1" ? "一级菜单" : "二级菜单"}
					},{
						title : '操作',
						toolbar : '#barDemo',
						align : 'center',
						width : 160,
					} ] 
				 ],
				 page: {
						layout: ['prev', 'page', 'next', 'skip', 'count', 'limit'],
						groups: 5,
						limit: 6,
						limits: [5, 10, 30, 40, 50],
						theme: '#1E9FFF',						
				 },
			});
			/*加载表格结束*/
		}
		
		//table监听工具条
		table.on('tool(powerinfo)', function(obj){ //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
		  var data = obj.data; //获得当前行数据
		  var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
		  var tr = obj.tr; //获得当前行 tr 的DOM对象
		  var roleid = $("#userRole").val();
		  
		  if(layEvent === 'add'){ //赋予权限
		      	$.post(urlapi+'systemmodel/addrolepower',{sysid:obj.data.sysid,roleid:roleid},function(flag){
		      		//alert(flag.code)
	    			if(flag.code == 0){
	    				layer.msg(flag.msg,{icon:6});
	    			}else{
	    				layer.msg(flag.msg,{icon:5});
	    				return;
	    			}
		    	});
		  }
	});		
});