layui.use([ 'table', 'form', 'layer', 'laydate', 'laytpl', 'element' ],function() {
	var table = layui.table;
	form = layui.form, layer = layui.layer,
	laydate = layui.laydate, laytpl = layui.laytpl,
	element = layui.element;
	
	//颜色区别
	function localFormatter(value) {
	    if (value == 1) {
	        return "<span class='label'  style='color:#ff8080'>已授予</span>";
	    } else if (value == 0) {
	        return "<span class='label'  style='color:#8080ff'>未授予</span>";
	    } 
	}
	
	//页面加载查询
	table.render({
		elem: '#backrolesystemmodel',
		id:'backrolesystemmodel',
		url: urlapi+'systemmodel/getRoleSystemModelByPage',
		loading: true,
		title: '角色权限表数据表',
		skin: 'line',
		height:'full-125',
		even: true,
		cols: [
			[
			{
				type : 'numbers',
				title: '序号',
				align: 'center',
				field: 'id',
				width: 80
			}, 
			{
				field: 'roleid',
				align: 'center',
				title: '角色',
				hide : true,
				width: 80
			},
			{
				field: 'sysid',
				align: 'center',
				title: '模块',
				hide : true,
				width: 80
			},
			{
				field: 'rolename',
				align: 'center',
				title: '角色名',	
				width: 200
			}, 
			{
				field: 'chinesename',
				align: 'center',
				title: '菜单名称',
				width:  200
			}, {
				field: 'deepth',
				align: 'center',
				templet:function(d){
		    		 return d.deepth == "1" ? "一级菜单" : "二级菜单"},
				title: '菜单层次',
				width:  240
			}, 
			{
				field: 'sysname',
				align: 'center',
				title: '父菜单名称',
				width:  200
			}, {
				field: 'isedit',
				align: 'center',
				title: '设置权限',
				templet:function(d){
	 			 return localFormatter(d.isedit)},
				width: 200
			},{
				title:'操作',
				align:'center',
				 toolbar: '#barDemo', 
				 width:200,
			}]
		],
		page: {
				layout: ['prev', 'page', 'next', 'skip', 'count', 'limit'],
				groups: 5,
				limit: 10,
				limits: [10, 20, 30, 40, 50],
				theme: '#1E9FFF',							
				}
	});
	
	//点击查询，更具角色筛选角色权限
	$("#btnselbackrole").click(function(){
		var roleid = $("#userRole").val();
		var roletext=$("#userRole  option:selected").text();
		//alert(roleid);
		if(roleid==""){
			layer.msg("请选择角色");
			return;
		}
		if(roleid=="undefined"){
			layer.msg("请选择角色");
			return;
		}
		table.render({
			elem: '#backrolesystemmodel',
			id:'backrolesystemmodel',
			url: urlapi+'systemmodel/getRoleSystemModelByRoleid?roleid=' + roleid,
			loading: true,
			title: '角色权限表数据表',
			skin: 'line',
			height:'full-125',
			even: true,
			cols: [
				[
				{
					type : 'numbers',
					title: '序号',
					align: 'center',
					field: 'id',
					width: 80
				}, 
				{
					field: 'roleid',
					align: 'center',
					title: '角色',
					hide : true,
					width: 80
				},
				{
					field: 'sysid',
					align: 'center',
					title: '模块',
					hide : true,
					width: 80
				},
				{
					field: 'rolename',
					align: 'center',
					title: '角色名',	
					width: 200
				}, 
				{
					field: 'chinesename',
					align: 'center',
					title: '菜单名称',
					width:  200
				}, {
					field: 'deepth',
					align: 'center',
					templet:function(d){
			    		 return d.deepth == "1" ? "一级菜单" : "二级菜单"},
					title: '菜单层次',
					width:  240
				}, 
				{
					field: 'sysname',
					align: 'center',
					title: '父菜单名称',
					width:  200
				}, {
					field: 'isedit',
					align: 'center',
					title: '设置权限',
					templet:function(d){
		 			 return localFormatter(d.isedit)},
					width: 200
				},{
					title:'操作',
					align:'center', 
					toolbar: '#barDemo',
					width:200,
				}]
			],page: {
				layout: ['prev', 'page', 'next', 'skip', 'count', 'limit'],
				groups: 5,
				limit: 10,
				limits: [10, 20, 30, 40, 50],
				theme: '#1E9FFF',							
				}
		});
	});
	
	//给用户添加权限操作
	$("#addRolepower").click(function(){
		var roleid = $("#userRole").val();
		var roletext=$("#userRole  option:selected").text();
		if(roleid==""){
			layer.msg("请选择角色");
			return;
		}
		if(roleid=="undefined"){
			layer.msg("请选择角色");
			return;
		}
		 layer.open({
             type: 2,
             title: '设置权限',
             shadeClose: true,
             shade: 0.5,
             closeBtn:'1',//右上角xx关闭
             area: ['700px','600px'],
             content:'addRolePower.html',
             btn: ['确定', '返回'],
             yes: function(index, layero){
		           //点击确认触发 iframe 内容中的按钮提交
		           var submit = layero.find('iframe').contents().find("#layuiadmin-app-form-submit");
		           submit.click();
	           },
             success : function(layero, index){
		            var body = layui.layer.getChildFrame('body', index);
		            body.find("#userRole").val(roleid);
		            setTimeout(function(){
		                layui.layer.tips('点击此处返回用户列表', '.layui-layer-setwin .layui-layer-close', {
		                    tips: 3
		                });
		            },100)
		        }
       });
	})
	
	//table监听工具条
	table.on('tool(backrolesystemmodel)', function(obj){ //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
	    var data = obj.data; //获得当前行数据
	    var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
	    var tr = obj.tr; //获得当前行 tr 的DOM对象
	 
		if(data.isedit == true){//只能取消
		 //alert(layEvent)
		 if(layEvent === 'edit'){
			 layer.msg("当前状态只能取消权限");
		     return;
		 }
		 layer.confirm('真的取消权限吗？', function(index){
		        //取消权限
		      	$.post(urlapi+'systemmodel/setrolepower',{id:obj.data.id,roleid:obj.data.roleid,sysid:obj.data.sysid,isedit:true},function(flag){
		      		//alert(flag.code)
					if(flag){
						layer.msg(flag.msg,{icon:6}); //表格刷新
						table.reload('backrolesystemmodel'); //重载表格	
					}else{
						layer.msg(flag.msg,{icon:5});
					}
		    	});
		});
	    }else if(data.isedit == false){//只能设置
		    if(layEvent === 'delete'){
				layer.msg("当前状态只能取消设置权限");
				return;
			}
		    $.post(urlapi+'systemmodel/setrolepower',{id:obj.data.id,roleid:obj.data.roleid,sysid:obj.data.sysid,isedit:false},function(flag){
	      		//alert(flag.code)
				if(flag){
					layer.msg(flag.msg,{icon:6}); //表格刷新
					table.reload('backrolesystemmodel'); //重载表格	
				}else{
					layer.msg(flag.msg,{icon:5});
				}
	    	});
	    }
	})
});