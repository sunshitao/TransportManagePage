//查询开始
layui.use([ 'table', 'form', 'layer',  'laytpl', 'element','laydate' ], function() {
	var table = layui.table, form = layui.form, 
		layer = layui.layer, $ = layui.jquery,
		element = layui.element,laydate = layui.laydate;
	
	 //日期(添加用户部分)
	  laydate.render({
	    elem: '#createtime'
	  });
	
	/* 页面打开数据加载 */
	querylist();
		
	/* 点击查询对网站用户进行筛选 */
	$("#btnselbackdepart").click(function(){
		querylist();//调用局部刷新
	});
		
	function querylist(){
		/* 页面打开数据加载 */
		var departmentselecet=$("#departmentselecet").val();
		var departmentselecet2=$("#departmentselecet2").val();
		var loginstatus=$("#loginstatus").val();
		if(loginstatus == 1){
			var status = "登录中";
		}else if(loginstatus == 2){
			var status = "未登录";
		}
		var param = "?userid="+departmentselecet+"&rolename="+departmentselecet2+"&loginstatus="+status;
		//alert(param)
		/*加载表格*/
		table.render({
			elem : '#Userlist',
			id:'Userlist',
			url : urlapi+'user/getuserlist'+param,
			title : '',
			height : "full-160",
			skin : 'line',
			curr:1,
			even : true,
			cols: [
			[ 
			{
				field: 'userid',
				align: 'center',
				title: '用户名'						
			} ,
			{
				field: 'username',
				align: 'center',
				title: '昵称'						
			} ,
			{
				field: 'realname',
				align: 'center',
				title: '真实姓名'						
			} ,
			{
				field: 'pwd',
				hide : true,
				align: 'center',
				title: '密码'						
			},
			{
				field: 'phone',
				align: 'center',
				width : 150,
				title: '联系电话'						
			},
			{
				field: 'status',
				align: 'center',
				title: '状态'						
			},
			{
				field: 'lastlogindate',
				align: 'center',
				width : 180,
				title: '最后登录时间'						
			},
			{
				field: 'createtime',
				align: 'center',
				width : 180,
				title: '创建时间'						
			},
			{
				field: 'loginstatus',
				align: 'center',
				title: '登录状态'						
			},
			{
				field: 'note',
				align: 'center',
				title: '备注'						
			},
			 {
				title: '操作',
				toolbar: '#barDemo',
				align: 'center',
				width : 180
			}] 
			 ],
			page: {
				layout: ['prev', 'page', 'next', 'skip', 'count', 'limit'],
				groups: 5,
				limit: 10,
				limits: [10, 20, 30, 40, 50],
				theme: '#1E9FFF',						
			 },
		});
	}
	/*点击查询加载表格数据结束*/
	
	table.on('tool(Userlist)', function(obj){ //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
	    var data = obj.data; //获得当前行数据
	    var layEvent = obj.event;
	    //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
	    var tr = obj.tr; //获得当前行 tr 的DOM对象
	    if(layEvent == 'del'){ //删除
			layer.confirm('真的删除行么', function(index){
				obj.del(); //删除对应行（tr）的DOM结构，并更新缓存
				layer.close(index);
				//向服务端发送删除指令
				$.post(urlapi+"user/deleteuser",{userid:obj.data.userid},function(flag){
					if(flag){
						layer.msg("刪除成功",{icon:6}); //表格刷新
						table.reload('Userlist'); //重载表格	
					}else{
						layer.msg("刪除失敗",{icon:5});
					}
				});
			});
	   } else if(layEvent == 'edit'){ //编辑
		//do something
	   //脚本编辑弹出层
		var name = encodeURIComponent(data.toolName);
		//alert(name);
		layer.open({
		    type: 2,
		    title: '修改信息',
		    shadeClose: true,
		    shade: 0.5,
		    closeBtn:'1',//右上角xx关闭
		    area: ['500px','500px'],
		    content:'edit_User.html',
		    btn: ['确定', '取消'],
		    yes: function(index, layero){
		    //点击确认触发 iframe 内容中的按钮提交
		    var submit = layero.find('iframe').contents().find("#btn_update");
		    submit.click();
			},
		    success : function(layero, index){
				var body = layui.layer.getChildFrame('body', index);
				if(layEvent == 'edit'){
					// 取到弹出层里的元素，并把编辑的内容放进去
					body.find("#userid").val(obj.data.userid);  //将选中的数据的id传到编辑页面的隐藏域，便于根据ID修改数据
					body.find("#username").val(obj.data.username); 
					body.find("#realname").val(obj.data.realname);
					body.find("#userRole").val(obj.data.roleid);
					body.find("#phone").val(obj.data.phone);
					body.find("#note").val(obj.data.note);
					// 记得重新渲染表单
					form.render();
				}
				setTimeout(function(){
					layui.layer.tips('点击此处返回用户列表', '.layui-layer-setwin .layui-layer-close', {
						tips: 3
					});
				},100)
			}
		 });
		//同步更新缓存对应的值
		obj.update({
		  username: '123'
		  ,title: 'xxx'
		});
	  }
	});		
    //查询结束
	
	//添加一条方法开始
	$("#btn_addUser").click(function(){
		//获取
		var userid=$("#userid").val();
		var username=$("#username").val();
		var realname=$("#realname").val();
		var roleid=$("#userRole").val();
		var phone=$("#phone").val();	
		var createtime=$("#createtime").val();	
		var note=$("#note").val();	
		var pwd = 000000;
		var enpwd = hex_md5(fix(userid,pwd));	
		//构建参数 
		var obj = {
			"userid":userid,
			"username":username,
			"realname":realname,
			"roleid":roleid,
			"phone":phone,
			"createtime":createtime,
			"note":note,
			"pwd":enpwd
		}
		$.ajax({
			type : "POST",
			url : urlapi+'user/adduser',
			data : obj, //直接传对象参数 
			dataType : 'json',
			success : function(data) {
				layer.msg(data.msg,{icon:6}); //表格刷新
				table.reload('Userlist'); //重载表格	
			},
			error:function(){
				layer.msg(data.msg,{icon:5}); //表格刷新;
			}  
		});
		return false;
    });
	//添加结束
});