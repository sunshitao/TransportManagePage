layui.use([ 'table', 'form', 'layer',  'laytpl', 'element','laydate' ], function() {
	var table = layui.table, form = layui.form, 
		layer = layui.layer, $ = layui.jquery,
		element = layui.element,laydate = layui.laydate;
	
	//查询角色下拉框
	$.ajax({
		url: urlapi+'role/getrolelist',
		type: 'get',
		xhrFields: {
		 withCredentials: true
		},
		crossDomain: true,
		success: function(roledata) {
			//console.log(roledata);
			if(roledata.code == 0) {
				var str = "";
				for(var i = 0; i < roledata.data.length; i++) {
					str += '<option value=' + roledata.data[i].roleid + '>' + roledata.data[i].rolename + '</option>';
				}
				 $("#userRole").append(str);
				form.render();
			} else {
					layer.msg("角色信息获取失败");
			}
		},
		error: function() {
			layer.msg("角色信息获取失败");
		}
	});
});