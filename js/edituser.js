layui.use(['table','jquery','form','layer'], function(){
	var table = layui.table;
	var $ = layui.jquery;
	var form=layui.form;
	var layer = layui.layer;
	//监听提交,添加事件
	form.on('submit(btn_update)', function(data){
	  var field = data.field; //获取提交的字段
	  var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引  
			var userid=$("#userid").val();
			var username=$("#username").val();
			var realname=$("#realname").val();
			var roleid=$("#userRole").val();
			var phone=$("#phone").val();	
			var note=$("#note").val();	
			var param ="?userid="+userid+ "&username=" + username + "&realname=" +realname + "&roleid=" +roleid + "&phone=" +phone + "&note=" +note;
			//alert(param)
			$.ajax({
				type:"get",
				url:urlapi+'user/modifyuser' + param,
				xhrFields: {
				 withCredentials: true
				},
				crossDomain: true,
				data:null,
				dataType : "json",
				success:function(data){
					if(data.code == 0){
						layer.msg(data.msg,{icon:6}); 
					}else{
						layer.msg(data.msg,{icon:5});
					}
					setTimeout(function(){
						parent.layui.table.reload('LAY-app-content-list'); //重载表格
						parent.layer.close(index); //再执行关闭 
						parent.layui.table.reload('Userlist'); //重载表格
					},1*1000)
				},
				error:function(data){
					layer.msg(data.msg,{icon:5});
				}
			})
		return false;
	});
});