//查询开始
layui.use([ 'table', 'form', 'layer',  'laytpl', 'element','laydate' ], function() {
	var table = layui.table, form = layui.form, 
		layer = layui.layer, $ = layui.jquery,
		element = layui.element,laydate = layui.laydate;
		
	$.ajax({
		url: urlapi+'car/ListCar',
		type: 'get',
		xhrFields: {
		 withCredentials: true
		},
		crossDomain: true,
		success: function(data) {
			console.log(data);
			if(data.code == 0) {
				var str = "";
				for(var i = 0; i < data.data.length; i++) {
					str += '<option value=' + data.data[i].carid + '>' + data.data[i].carid + '</option>';
				}
				 $("#carid").append(str);
				form.render();
			} else {
					layer.msg("车辆信息获取失败");
			}
		},
		error: function() {
			layer.msg("车辆信息获取失败");
		}
	});
	
	/* 页面打开数据加载 */
	querylist();
		
	/* 点击查询对网站用户进行筛选 */
	$("#btnselbackdepart").click(function(){
		querylist();//调用局部刷新
	});
		
	function querylist(){
		/* 页面打开数据加载 */
		var carid=$("#carid").val();
		var param = "?carid="+carid;
		//alert(param)
		/*加载表格*/
		table.render({
			elem : '#table',
			id:'table',
			url : urlapi+'task/monitoringTemp'+param,
			title : '',
			height : "full-160",
			skin : 'line',
			curr:1,
			even : true,
			cols: [
			[ 
			{
				field: 'taskid',
				align: 'center',
				title: '任务编号',
				hide : true,
			} ,
			{
				field: 'realname',
				align: 'center',
				title: '司机姓名'						
			} ,
			{
				field: 'phone',
				align: 'center',
				title: '联系方式'						
			} ,
			{
				field: 'carid',
				align: 'center',
				title: '车牌号'						
			},
			{
				field: 'cartepy',
				align: 'center',
				title: '车型'						
			},
			{
				field: 'routename',
				align: 'center',
				title: '线路名称'						
			},
			{
				field: 'tasknote',
				align: 'center',
				title: '工单状态'						
			},
			{
				field: 'maxtemp',
				align: 'center',
				title: '最高温度'						
			},
			{
				field: 'mintemp',
				align: 'center',
				title: '最低温度'						
			},
			 {
				title: '操作',
				toolbar: '#barDemo',
				align: 'center',
				width : 180
			}] 
			 ],
			page: {
				layout: ['prev', 'page', 'next', 'skip', 'count', 'limit'],
				groups: 5,
				limit: 10,
				limits: [10, 20, 30, 40, 50],
				theme: '#1E9FFF',						
			 },
		});
	}
	/*点击查询加载表格数据结束*/
	
	table.on('tool(table)', function(obj){ //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
	    var data = obj.data; //获得当前行数据
	    var layEvent = obj.event;
	    //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
	    var tr = obj.tr; //获得当前行 tr 的DOM对象
	    if(layEvent == 'del'){ //删除
			layer.confirm('真的删除行么', function(index){
				obj.del(); //删除对应行（tr）的DOM结构，并更新缓存
				layer.close(index);
				//向服务端发送删除指令
				$.post(urlapi+"user/deleteuser",{userid:obj.data.userid},function(flag){
					if(flag){
						layer.msg("刪除成功",{icon:6}); //表格刷新
						table.reload('table'); //重载表格	
					}else{
						layer.msg("刪除失敗",{icon:5});
					}
				});
			});
	   } else if(layEvent == 'edit'){ //编辑
		//do something
	   //脚本编辑弹出层
		var name = encodeURIComponent(data.toolName);
		//alert(name);
		layer.open({
		    type: 2,
		    title: '获取联系方式',
		    shadeClose: true,
		    shade: 0.5,
		    closeBtn:'1',//右上角xx关闭
		    area: ['400px','350px'],
		    content:'Phone.html',
		    //btn: ['确定', '取消'],
		    yes: function(index, layero){
		    //点击确认触发 iframe 内容中的按钮提交
		    var submit = layero.find('iframe').contents().find("#btn_update");
		    submit.click();
			},
		    success : function(layero, index){
				var body = layui.layer.getChildFrame('body', index);
				if(layEvent == 'edit'){
					// 取到弹出层里的元素，并把编辑的内容放进去
					body.find("#realname").val(obj.data.realname);//将选中的数据的id传到编辑页面的隐藏域，便于根据ID修改数据
					body.find("#phone").val(obj.data.phone);  
					// 记得重新渲染表单
					form.render();
				}
				setTimeout(function(){
					layui.layer.tips('点击此处返回用户列表', '.layui-layer-setwin .layui-layer-close', {
						tips: 3
					});
				},100)
			}
		 });
		//同步更新缓存对应的值
		obj.update({
		  username: '123'
		  ,title: 'xxx'
		});
	  }
	});		
    //查询结束
});