layui.use([ 'table', 'form', 'layer', 'laydate','element' ],function() {
	var table = layui.table;
	form = layui.form, layer = layui.layer,
	laydate = layui.laydate, laytpl = layui.laytpl,
	element = layui.element,layedit=layui.layedit;
	
	/*layedit.build('content', {
		height: 180 //设置编辑器高度
		, tool: ['']
	});*/
	

	/*
	 * 点击查询，更具角色筛选角色权限
	 */
	$("#btnselbackrole").click(function(){ 
		var payid = $("#payid").val();
		var sortid = $("#sortid").val();
		var datetime = $("#datetime").val();
		var remark = $("#opreation").val().trim();
		table.reload('backrolesys', {
				method : 'post',
				where : {
					payid:payid,
					sortid:sortid,
					datetime:datetime,
					remark: remark
						},
				page : {
					curr : 1
					}
			});
	});
	
	
	/*
	 * 日常生活状态
	 */
	$("#btn_income").click(function(){
		getsorttypr(1);
		layer.open({
			title:"添加页",
			type: 1,
			area: ['700px'],
			skin: 'demo-class',
			maxmin: true,//显示最大化最小化按钮
			content: $('#div_addmajor'),
			btn1: function(index, layero){
				$.ajax({
	        		type: 'get',
	        		url: "bookkeep/addbookeep",
	        		dataType: 'json',
	        		data:{
	        			money:money,
	        			datetime:datetime,
	        			payid:payid,
	        			sortid:sortid,
	        			type:1,
	        			remark:remark
	        		},
	        		success:function(data){
	        			if(data.code == 0){
	        				layer.confirm(data.msg, {
	        				icon: 1,
							  btn: ['确定']
							}, function(){
								table.reload("backrolesys", { //此处是上文提到的 初始化标识id
					                where: {
					                	
					                },page: {
					                curr:1
					                }
					            });	
								layer.closeAll();
							});          				 
	        			}
	        			else{
	        				layer.confirm(data.msg, {
	        					  icon: 6,
								  btn: ['确定']
							});
	        			}
	        		},
	        		
	        	});  
			},
			cancel: function(){ 
				$('#money').val("");
				$('#remark').val("");
				$("#date").val("");
			}
		});
	})
	/* $(document).ready(function(){
		$.ajax({
		    url:'bookkeep/getallsort',
		    dataType:'json',
		    type:'post',
		    success:function(succ){
		    	var data = succ.data;
		        for(var i=0;i<data.length;i++){
		        	var append = "<option value='"+data[i].sortTypeId+"'><img src='"+data[i].photoname+"' style='width:30px;'>"+data[i].sortname+"</option>"
					$("#sortid").append(append);
		        }
		        form.render();//菜单渲染 把内容加载进去
		    },error:function(err){
		    	layer.msg(err.msg);
		    }
		})
	}) */
	/*
	 * 收入获取分类类型
	 */
	function getsorttypr(type){
		$.ajax({
    		type: 'get',
    		url: "bookkeep/getsort",
    		data:{
    			type:type
    		},
    		dataType: 'json',
    		success:function(succ){
    			if(succ.code == 0){
    				$("#type1").empty();
    				var data = succ.data;
    				for(var i=0;i<data.length;i++){
    					var append = "<input type='radio' name='sort' value='"+data[i].sortTypeId+"' title='"+data[i].sortname+"'>"+"<img src='"+data[i].photoname+"' style='width:30px;'>"
    					$("#type1").append(append);
    				}
    				form.render();
    			}
    			else{
    				layer.confirm(succ.msg, {
      				  icon: 6,
					  btn: ['确定']
    				});
    			}
    		},
    		
    	});
	}
	/*
	 * 编辑获取分类类型
	 */
	function geteditsorttypr(type){
		$.ajax({
    		type: 'get',
    		url: "bookkeep/getsort",
    		data:{
    			type:type
    		},
    		dataType: 'json',
    		success:function(succ){
    			if(succ.code == 0){
    				$("#type3").empty();
    				var data = succ.data;
    				for(var i=0;i<data.length;i++){
    					var append = "<input type='radio' name='editsort' value='"+data[i].sortTypeId+"' title='"+data[i].sortname+"'>"+"<img src='"+data[i].photoname+"' style='width:30px;'>"
    					$("#type3").append(append);
    				}
    				form.render();
    			}
    			else{
    				layer.confirm(succ.msg, {
      				  icon: 6,
					  btn: ['确定']
    				});
    			}
    		},
    		error:function(){
    			layer.confirm('出现错误，请重试！', {
				  icon: 6,
				  btn: ['确定']
				});
    		}
    	});
	}
});
