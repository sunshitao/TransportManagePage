//查询开始
	layui.use([ 'table', 'form', 'layer',  'laytpl', 'element' ], function() {
			var table = layui.table, form = layui.form, 
				layer = layui.layer, $ = layui.jquery,
				element = layui.element;
				
			/*加载表格*/
			table.render({
				elem : '#Rolemodel',
				id:'Rolemodel',
				url : urlapi+'role/getrolelistByPage',
				title : '',
				height : "full-160",
				skin : 'line',
				curr:1,
				even : true,
				cols : [ 
				     [{
						field : 'roleid',
						align : 'center',
						title : '角色编号',
						
					},
				     {
						field : 'rolename',
						align : 'center',
						title : '角色名称',
						
					}, {
						field : 'description',
						align : 'center',
						title : '角色描述',
							
					},
					{
						field : 'createtime',
						align : 'center',
						title : '创建时间',
							
					},
					{
						field : 'deepth',
						align : 'center',
						title : '深度',
							
					},
					{
						field : 'parentid',
						align : 'center',
						title : '父级ID',
							
					},
					 {
							title: '操作',
							toolbar: '#barDemo',
							align: 'center',
							width : 180
						}] 
				 ],
				page: {
						layout: ['prev', 'page', 'next', 'skip', 'count', 'limit'],
						groups: 5,
						limit: 10,
						limits: [10, 20, 30, 40, 50],
						theme: '#1E9FFF',						
				 }
			});
			
			//条件查询
			$("#selectRole").click(function(){
	   		var rolename=$("#departmentselecet").val().trim();
	   		var param = '?rolename='+rolename;
	   		table.render({
				elem : '#Rolemodel',
				id:'Rolemodel',
				url : urlapi+'role/getrolelistByPage' + param,
				title : '',
				height : "full-160",
				skin : 'line',
				curr:1,
				even : true,
				cols : [ 
				     [{
						field : 'roleid',
						align : 'center',
						title : '角色编号',
						
					},
				     {
						field : 'rolename',
						align : 'center',
						title : '角色名称',
						
					}, {
						field : 'description',
						align : 'center',
						title : '角色描述',
							
					},
					{
						field : 'createtime',
						align : 'center',
						title : '创建时间',
							
					},
					{
						field : 'deepth',
						align : 'center',
						title : '深度',
							
					},
					{
						field : 'parentid',
						align : 'center',
						title : '父级ID',
							
					},
					 {
							title: '操作',
							toolbar: '#barDemo',
							align: 'center',
							width : 180
						}] 
				 ],
				page: {
						layout: ['prev', 'page', 'next', 'skip', 'count', 'limit'],
						groups: 5,
						limit: 10,
						limits: [10, 20, 30, 40, 50],
						theme: '#1E9FFF',						
				 }
			});
	   });
			
			table.on('tool(Rolemodel)', function(obj){ //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
			  var data = obj.data; //获得当前行数据
			  var layEvent = obj.event;
			  //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
			  var tr = obj.tr; //获得当前行 tr 的DOM对象
			  if(layEvent == 'del'){ //删除
			    layer.confirm('真的删除行么', function(index){
			      obj.del(); //删除对应行（tr）的DOM结构，并更新缓存
			      layer.close(index);
			      //向服务端发送删除指令
			      	$.post(urlapi+"role/deleterole",{roleid:obj.data.roleid},function(flag){
		    			if(flag){
		    				layer.msg("刪除成功",{icon:6}); //表格刷新
					        table.reload('Rolemodel'); //重载表格	
		    			}else{
		    				layer.msg("刪除失敗",{icon:5});
		    			}
			    	});
			    });
			   } else if(layEvent == 'edit'){ //编辑
			    //do something
			   //脚本编辑弹出层
		             var name = encodeURIComponent(data.toolName);
		             //alert(name);
		             layer.open({
			               type: 2,
			               title: '修改信息',
			               shadeClose: true,
			               shade: 0.5,
			               closeBtn:'1',//右上角xx关闭
			               area: ['500px','500px'],
			               content:'edit_Role.html',
			               btn: ['确定', '取消'],
			               yes: function(index, layero){
				           var submit = layero.find('iframe').contents().find("#btn_update");
				           submit.click();
				           //表格刷新
				        },
			               success : function(layero, index){
					            var body = layui.layer.getChildFrame('body', index);
					            if(layEvent == 'edit'){
					                // 取到弹出层里的元素，并把编辑的内容放进去
					                body.find("#Roleid").val(obj.data.roleid);  //将选中的数据的id传到编辑页面的隐藏域，便于根据ID修改数据
					                body.find("#Rolename").val(obj.data.rolename); 
					                body.find("#description").val(obj.data.description);
					                body.find("#createtime").val(obj.data.createtime);
					                body.find("#deepth").val(obj.data.deepth);
					                body.find("#parentid").val(obj.data.parentid);
					                // 记得重新渲染表单
			                		form.render();
					            }
					            setTimeout(function(){
					                layui.layer.tips('点击此处返回用户列表', '.layui-layer-setwin .layui-layer-close', {
					                    tips: 3
					                });
					            },100)
					        }
		             });
		              //同步更新缓存对应的值
			    obj.update({
			      username: '123'
			      ,title: 'xxx'
			    });
			  }
			});		
		//查询结束
		
		//添加一条方法开始
		$("#btn_addroler").click(function(){
				var Rolename=$("#Rolename").val();
				var description=$("#description").val();
				var deepth=$("#deepth").val();	
				var parentid=$("#parentid").val();
				if(Rolename == "") {
					layer.tips('不能为空', '#Rolename');
					return;
				} 
				if(description==""){
					ayer.tips('不能为空', '#description');
					return;
				}
				if(deepth==""){
					layer.tips('不能为空', '#deepth');
					return;
				}
				if(parentid==""){
					layer.tips('不能为空', '#parentid');
					return;
				}
				var obj = {
							"Rolename":Rolename,
							"description":description,
							"deepth":deepth,	
							"parentid":parentid,						
							};
				$.ajax({
						url : urlapi+'role/addRole',
						type : "POST",
						data : obj, //直接传对象参数 
						dataType : 'json',
						success : function(data) {
							layer.msg("添加成功",{icon:6}); //表格刷新
							table.reload('Rolemodel'); //重载表格	
						},
						error:function(){
							layer.msg("添加失败",{icon:5}); //表格刷新;
						}  
					})
				});
			});
			//添加结束