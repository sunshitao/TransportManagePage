//查询开始
		layui.use([ 'table', 'form', 'layer',  'laytpl', 'element' ], function() {
			var table = layui.table, form = layui.form, 
				layer = layui.layer, $ = layui.jquery,
				element = layui.element;
			/*加载表格*/
			table.render({
				elem :'#CarList',
				id:'CarList',
				url : urlapi+'car/getcarlist',
				title : '',
				height : "full-160",
				skin : 'line',
				curr:1,
				even : true,
				cols : [ 
						[{
						field : 'carid',
						align : 'center',
						title : '车牌',	
						},{
							field : 'cartepy',
							align : 'center',
							title : '车型',
						}, {
							field : 'maintenance',
							align : 'center',
							title : '保养记录',
						},{
							field : 'isenable',
							align : 'center',
							title : '启用状态',
						},{
							field : 'isedit',
							align : 'center',
							title : '使用状态',
						},{
							field : 'jointime',
							align : 'center',
							title : '购车时间',
						},{
							field : 'note',
							align : 'center',
							title : '基本详情',	
						},{
							field : 'inspect',
							align : 'center',
							title : '年检时间',
						},
						 {
							title: '操作',
							toolbar: '#barDemo',
							align: 'center',
							width : 180
						}] 
						],
						page: {
							layout: ['prev', 'page', 'next', 'skip', 'count', 'limit'],
							groups: 5,
							limit: 10,
							limits: [10, 20, 30, 40, 50],
							theme: '#1E9FFF',							
					}
			});
			
			//条件查询
			$("#btnselbackrole").click(function(){
	   		var carid=$("#carid").val().trim();
	   		var param = '?carid='+carid;

	   		table.render({
	   			elem :'#CarList',
	   			id:'CarList',
	   			url : urlapi+'car/getcarlist'+param,
	   			title : '',
	   			height : "full-160",
	   			skin : 'line',
	   			curr:1,
	   			even : true,
	   			cols : [ 
	   					[{
	   					field : 'carid',
	   					align : 'center',
	   					title : '车牌',	
	   					},{
	   						field : 'cartepy',
	   						align : 'center',
	   						title : '车型',
	   					}, {
	   						field : 'maintenance',
	   						align : 'center',
	   						title : '保养记录',
	   					},{
	   						field : 'isenable',
	   						align : 'center',
	   						title : '启用状态',
	   					},{
	   						field : 'isedit',
	   						align : 'center',
	   						title : '使用状态',
	   					},{
	   						field : 'jointime',
	   						align : 'center',
	   						title : '购车时间',
	   					},{
	   						field : 'note',
	   						align : 'center',
	   						title : '基本详情',	
	   					},{
	   						field : 'inspect',
	   						align : 'center',
	   						title : '年检时间',
	   					},
	   					 {
	   						title: '操作',
	   						toolbar: '#barDemo',
	   						align: 'center',
	   						width : 180
	   					}] 
	   					],
	   					page: {
	   						layout: ['prev', 'page', 'next', 'skip', 'count', 'limit'],
	   						groups: 5,
	   						limit: 10,
	   						limits: [10, 20, 30, 40, 50],
	   						theme: '#1E9FFF',						
	   				}
	   		});
	   });
			//菜单编辑
			table.on('tool(CarList)', function(obj){ //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
				var data = obj.data; //获得当前行数据
				var layEvent = obj.event;
				//获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
				var tr = obj.tr; //获得当前行 tr 的DOM对象
				if(layEvent == 'del'){ //删除
			    layer.confirm('真的删除行么', function(index){
					obj.del(); //删除对应行（tr）的DOM结构，并更新缓存
					layer.close(index);
					//向服务端发送删除指令
			      	$.post(urlapi+"car/deletecar",{carid:obj.data.carid},function(flag){
		    			if(flag){
		    				layer.msg("刪除成功",{icon:6}); //表格刷新
					        table.reload('CarList'); //重载表格	
		    			}else{
		    				layer.msg("刪除失敗",{icon:5});
		    			}
			    	});
			    });
			   } else if(layEvent == 'edit'){ //编辑
			    //do something
			   //脚本编辑弹出层
		             var name = encodeURIComponent(data.toolName);
		             //alert(name);
		             layer.open({
			               type: 2,
			               title: '修改信息',
			               shadeClose: true,
			               shade: 0.5,
			               closeBtn:'1',//右上角xx关闭
			               area: ['500px','500px'],
			               content:'edit_Car.html',
			               btn: ['确定', '取消'],
			               yes: function(index, layero){
				           //点击确认触发 iframe 内容中的按钮提交
				           var submit = layero.find('iframe').contents().find("#btn_update");
				           submit.click();
				           //表格刷新
				        },
			               success : function(layero, index){
					            var body = layui.layer.getChildFrame('body', index);
					            if(layEvent == 'edit'){
					                // 取到弹出层里的元素，并把编辑的内容放进去
					                body.find("#carid").val(obj.data.carid);  //将选中的数据的id传到编辑页面的隐藏域，便于根据ID修改数据
					                body.find("#cartepy").val(obj.data.cartepy); 
					                body.find("#maintenance").val(obj.data.maintenance);
					                body.find("#jointime").val(obj.data.jointime);
									body.find("#note").val(obj.data.note);
									body.find("#inspect").val(obj.data.inspect);
					                // 记得重新渲染表单
			                		form.render();
					            }
					            setTimeout(function(){
					                layui.layer.tips('点击此处返回用户列表', '.layui-layer-setwin .layui-layer-close', {
					                    tips: 3
					                });
					            },100)
					        }
		             });
		              //同步更新缓存对应的值
			    obj.update({
			    });
			  }
			});		
			
		//查询结束
		$("#btn_addUser").click(function(){
					var carid=$("#carid").val();
					var cartepy=$("#cartepy").val();
					var maintenance=$("#maintenance").val();	
					var jointime=$("#jointime").val();
					var note=$("#note").val();
					var inspect =$("#inspect").val();	
					if(carid == "") {
						layer.tips('不能为空', '#carid');
						return;
					} 
					if(cartepy==""){
						layer.tips('不能为空', '#cartepy');
						return;
					}
					if(maintenance==""){
						layer.tips('不能为空', '#maintenance');
						return;
					}
					if(jointime==""){
						layer.tips('不能为空', '#jointime');
						return;
					}
					if(inspect==""){
						layer.tips('不能为空', '#inspect');
						return;
					}
						var obj = {
							"carid":carid,
							"cartepy":cartepy,
							"maintenance":maintenance,
							"jointime":jointime,	
							"note":note,		
							"inspect":inspect
						};
						$.ajax({
							url : urlapi+'car/addcar',
							type : "POST",
							data : obj, //直接传对象参数 
							dataType : 'json',
							success : function(data) {
								layer.msg("添加成功",{icon:6}); //表格刷新
								table.reload('CarList'); //重载表格	
							},
							error:function(){
								layer.msg("添加失败",{icon:5}); //表格刷新;
							}  
						})
					
					});
				});	
	 